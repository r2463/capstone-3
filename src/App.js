import { useReducer } from 'react'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import { ToastContainer } from 'react-toastify'

// IÂPORT CSS
import 'react-toastify/dist/ReactToastify.css'


// IMPORT PAGES
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products'
import SingleProduct from './pages/SingleProduct'
import CreateProduct from './pages/CreateProduct'
import UpdateProduct from './pages/UpdateProduct'
import Cart from './pages/Cart'
import ErrorPage from './pages/ErrorPage'
import { UserProvider } from './UserContext'
import { reducer, initialState } from './reducer/userReducer'


// IMPORT COMPONENTS
import AppNavBar from './components/AppNavbar';
import Footer from './components/Footer'
import AdminView from './pages/AdminView'




function App() {


  const [state, dispatch] = useReducer(reducer, initialState)
  console.log(state)





  return (
    
    <UserProvider value={{state, dispatch}}>
    <BrowserRouter>
    <ToastContainer/>
    <AppNavBar/>
      <Routes>
          <Route path="/" element={ <Home/> }/>
          <Route path="/register" element={ <Register/> }/>
          <Route path="/login" element={ <Login/> }/>
          <Route path="/logout" element={ <Logout/> }/>
          <Route path="/products" element={ <Products/> }/>
          <Route path="/products/:productId" element={ <SingleProduct/> }/>
          <Route path="/create" element={ <CreateProduct/> }/>
          <Route path="/products/:productId/update" element={ <UpdateProduct/> }/>
          <Route path="/products" element={ <AdminView/> }/>
          <Route path="/cart" element={ <Cart/> }/>
          <Route path="*" element={ <ErrorPage/>  }/>
        </Routes>
    <Footer/>
    </BrowserRouter>
    </UserProvider>
    


  );
}

export default App;
