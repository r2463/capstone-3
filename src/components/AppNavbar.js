import { Fragment, useContext, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Navbar, Container, Nav } from 'react-bootstrap'
import { Badge } from '@mui/material';
import {ShoppingCartOutlined} from '@mui/icons-material'
import UserContext from '../UserContext'



export default function AppNavBar(){

  
  const cartQuantity = useSelector(state => state.cart.cartQuantity)
  // console.log(cartQuantity)

  const { state, dispatch } = useContext(UserContext)
  // console.log(state)


  

  const NavLinks = () => {

    if(state === true){
      return(
        <Fragment>

            <Link to='/cart' className="navLink">
            <Badge badgeContent={cartQuantity}
            sx={{
              "& .MuiBadge-badge": {
              color: "white",
              backgroundColor: "red"
            }
          }}>
            <ShoppingCartOutlined className='cartIcon'/>
            </Badge>
          </Link> 

          <Link 
            to="/logout" 
            className="navLink text-light">Logout</Link>

        </Fragment>
      )
    } else {
      return(
        <Fragment>
            <Link 
                to="/login" 
                className=" navLink text-light">Login</Link>
            <Link 
                to="/register" 
                className=" navLink text-light">Register</Link>
        </Fragment>
      )
    }
  }






    return(

        <Navbar  id='navbar' >
            <Navbar.Brand href="#home" className='text-light'>Ghost Kitchen</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" variant='light'/>
            <Navbar.Collapse id="basic-navbar-nav" >
            <>
              <Nav className="nav container-fluid">
                <Nav.Item className='navItem-left'>
                  <Link to='/' className='navLink text-light'>Home</Link>
                  <Link to="/products" className='navLink text-light'>Products</Link>
                </Nav.Item>
                <Nav.Item className='navItem-right'>
                  <NavLinks />
                </Nav.Item>
              </Nav>
              </>
            </Navbar.Collapse>
        </Navbar>


    )
}