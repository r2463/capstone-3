import { Link } from 'react-router-dom'
import { Card, Button, Container, Row, Col, } from 'react-bootstrap'
import pasta from '../images/pasta.png'


export default function ProductCard({productProp}){

  const {name, description, price, _id} = productProp
    // console.log(name)
    // console.log(description)
    // console.log(price)



    return(

          <Col>
          <Card className=" productCard m-3">
          <Link to={`/products/${_id}`}>
          <Card.Img variant="top" src={pasta} className='img-fluid'/>
          </Link>
          <Card.Body>
            <Link to={`/products/${_id}`}>
            <Card.Title className='productTitle'>{name}</Card.Title>
            </Link>

            <Card.Text>{description}</Card.Text>

            <div className='text-left mt-5'>
              <Card.Subtitle>Price</Card.Subtitle>
              <Card.Text className='productPrice'>{price}</Card.Text>
            </div>
          </Card.Body>
          </Card>
          </Col>



    )
}