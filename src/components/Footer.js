import {Facebook, Instagram, Twitter, RoomOutlined, Phone, MailOutline } from '@mui/icons-material'
import payment from '../images/payment.png'


export default function Footer(){

    return(

        <div className=" footer text-white d-flex fixed-bottom py-4">
            <div className="footer-1 p-3">
                <h1 className='text-center'>Ghost Kitchen</h1>
                <div className='footer-1Details'>
                    <p>Trayed meals for your celebrations and food cravings. Follow us on our socials! </p>
                </div>

                <div className='socialIcons d-flex'>
                    <a className='icon1' href='https://www.facebook.com/GhostKitchenPH' target="_blank" rel="noreferrer"><Facebook/></a>
                    <a className='icon2' href="https://www.instagram.com/ghostkitchenph/" target="_blank" rel="noreferrer"><Instagram/></a>
                    <a className='icon3' href='#'><Twitter/></a>

                </div>
            </div>

            <div className="footer-2 p-3 text-center">
                <h4>Useful Links</h4>
                <ul className=' m-3'>
                    <li>
                        <a className='anchor' href='/'>Home</a>
                    </li>
                    <li>
                        <a href='./Products'>Products</a>
                    </li>
                    <li>
                        <a>My Account</a>
                    </li>
                    <li>
                        <a href='./cart'>Cart</a>
                    </li>
                </ul>
            </div>

            <div className="footer-3 p-3">
                <h4 className='mb-3'>Contact Us</h4>
                <div><RoomOutlined/> 124-D Estacio St. Napindan, Taguig City</div>
                <div><Phone/>0961 579 4566</div>
                <div><MailOutline/>ghostkitchenph@mail.com</div>
                <div className=''>
                <img src={payment} className='payment img-fluid' alt='payment.png'/>
                </div>
            </div>
        </div>

    )
}