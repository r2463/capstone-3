


export default function Banner({bannerProp}){

    // console.log(bannerProp)

    const {title, description, destination, buttonDesc} = bannerProp

    return(

        <div className="jumbotron jumbotron-fluid">
        <div className="jumbotronContainer">
            <h1 className="jumbotron-title">{title}</h1>
            <p className="lead">{description}</p>
            <div className="jumbotronButton">
                <a className="jumbotronButtonDesc" href={destination}>{buttonDesc}</a>
            </div>
        </div>
        </div>


    )
}