import { createSlice} from '@reduxjs/toolkit';
import { toast } from 'react-toastify'


const cartSlice = createSlice({

    name: "cart",
    initialState: {
        products: [],
        cartQuantity: 0,
        totalPrice: 0
    },
    reducers:{
        addProduct: (state, action) => {

            // find if product already exists in cart
            const itemIndex = state.products.findIndex(product => product._id === action.payload._id);

            // If TRUE, just add quantity
            if(itemIndex >= 0){
                // console.log(action.payload)
                state.products[itemIndex].quantity += 1;
                toast.success(`${action.payload.name} added to Cart`, {position: "bottom-left"})

            }
            // if FALSE, add the new product to cart
            else{
                state.products.push(action.payload);
                state.cartQuantity += 1; //update cartQuantity state
                toast.success(`${action.payload.name} added to Cart`, {position: "bottom-left"})
            }
            
            // update totalPrice state
            state.totalPrice += action.payload.price *action.payload.quantity;   
            
            
            // state.quantity += 1;
            // state.products.push(action.payload);
            
        },
        
        removeFromCart: (state, action) => {
            
            // filter array of products to not include the item we select to remove
            const nextProductsItems = state.products.filter(
                (product) => product._id !== action.payload._id
                
                )
                
                state.products = nextProductsItems;
                state.cartQuantity -=1;
                state.totalPrice -= action.payload.price * action.payload.quantity;
                toast.error(`${action.payload.name} removed from Cart`, {position: "bottom-left"})
        },

        decreaseCart: (state, action) => {

            const itemIndex = state.products.findIndex(product => product._id === action.payload._id);

            if(state.products[itemIndex].quantity > 1){
                state.products[itemIndex].quantity -= 1;


            } else{

                const nextProductsItems = state.products.filter(
                    (product) => product._id !== action.payload._id
                    
                    )
                    
                    state.products = nextProductsItems;
                    state.cartQuantity -=1;
                    toast.error(`${action.payload.name} removed from Cart`, {position: "bottom-left"})
            }
        
            state.totalPrice -= action.payload.price;
            
        },

        increaseCart: (state, action) => {
            
            const itemIndex = state.products.findIndex(product => product._id === action.payload._id);

                state.products[itemIndex].quantity += 1;
                state.totalPrice += action.payload.price;
            
        },

        clearCart: (state, action) => {

            state.products = [];
            state.cartQuantity = 0;
            state.totalPrice = 0;
            toast.error(`Cart Cleared`, {position: "bottom-left"}) 
        }
        
    }

});


export const { addProduct, removeFromCart, decreaseCart, increaseCart, clearCart } = cartSlice.actions;
export default cartSlice.reducer;
