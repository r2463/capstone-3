import { Fragment, useContext, useState, useEffect } from 'react'
import { Table, Container, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'


import UserContext from '../UserContext'






export default function AdminView(){



    const {state, dispatch} = useContext(UserContext)
    const [allProducts, setAllProducts] = useState([])

    
    const fetchData = () => {

        fetch('https://morning-spire-84095.herokuapp.com/api/products/', {

            method: "GET",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(response => {
            // console.log(response)

            dispatch({type: "USER", payload: true})

            setAllProducts(response.map(product => {

                return(

                    <tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
                        <td>
                        <Link 
                        className="btn btn-primary mx-2" to={product._id+'/update'}
                       >
                        Update
                        </Link> 
							{
								product.isActive ?
                                    <Fragment>                               
                                        <Button 
                                            className="btn btn-danger mx-2"
                                            onClick={ () => handleArchive(product._id) }
                                        >
                                            Archive
                                        </Button>
                                        </Fragment>
                                    :
                                        <Fragment>                                  
                                        <Button 
                                            className="btn btn-success mx-2"
                                            onClick={ () => handleUnarchive(product._id)}
                                        >
                                                Unarchive
                                        </Button>
                                        <Button 
                                            className="btn btn-secondary mx-2"
                                            onClick={ () => handleDelete(product._id) }
                                        >
                                            Delete
                                        </Button>
									</Fragment>
                                    
							}
						</td>
					</tr>
                )
            }))
        })
    }

    
    useEffect(() => {

        fetchData()
    }, [])


    // DEFINE handleEdit, handleArchive, handleUnarchive, handleDelete

    // const handleEdit =(productId) => {

    //     fetch (`http://localhost:3010/api/products/${productId}`, {

    //         method: "PUT",
    //         headers: {
    //             "Authorization": `Bearer ${localStorage.getItem('token')}`
    //         }
    //     })
    //     .then(response => response.json())
    //     .then(response => {

    //         if(response) {
                
    //             fetchData()
    //             alert(`${response.name} successfully Updated`)
    //         }
    //     })
    // }
   
    const handleArchive =(productId) => {

        fetch (`https://morning-spire-84095.herokuapp.com/api/products/${productId}/archive`, {

            method: "PATCH",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(response => {

            if(response) {
                
                fetchData()
            }
        })
    }

    const handleUnarchive =(productId) => {

        fetch (`https://morning-spire-84095.herokuapp.com/api/products/${productId}/unArchive`, {

            method: "PATCH",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(response => {

            if(response) {

                fetchData()
            }
        })
    }

    const handleDelete = (productId) => {

        fetch (`https://morning-spire-84095.herokuapp.com/api/products/${productId}/delete-product`, {

            method: "DELETE",
            headers: {
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(response => {
            console.log(response)

            if(response) {

                fetchData()
                alert(`${response.name} successfully Deleted`)
            }
        })
    }



   return(

    <Container className="container">
    <h1 className="my-5 text-center">Product Dashboard</h1>
    <div className="text-right">
        <Link className="btn btn-info m-2" to={'/create'}>Add Product</Link>
        <Link className="btn btn-info m-2" to={`#`}>User Dashboard</Link>
    </div>
    <Table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Product Name</th>
                <th>Price</th>
                <th>Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            { allProducts }
        </tbody>
    </Table>
    </Container>
 
   
    )
}