import { useState, useContext, useEffect } from 'react'
import { useNavigate, useParams, Link } from 'react-router-dom'
import { Container, Form, Row, Col,Button } from 'react-bootstrap'
import UserContext from '../UserContext'


const token = localStorage.getItem('token')


export default function UpdateProduct() {


    const {productId} = useParams()
    const navigate = useNavigate()


    const {state, dispatch} = useContext(UserContext)

    const[name, setName] = useState("")
    const[description, setDescription] = useState("")
    const[price, setPrice] = useState(0)
    const[isDisabled, setIsDisabled] = useState(true)

    
    // FETCHING PRODUCT DATA IN API
    const fetchProduct = () => {

        fetch(`https://morning-spire-84095.herokuapp.com/api/products/${productId}`, {

            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(response => {

            console.log(response)
            setName(response.name)
			setDescription(response.description)
			setPrice(response.price)

        })
    }

     // USE EFFECT HOOK TO AUTOFILL THE PAGE
     useEffect(() => {

        if(token !== null){

            dispatch({type: "USER", payload: true})

        }
        fetchProduct()
    }, [])    


    // APPLY CHANGES WHEN BUTTON IS SUBMITTED
    const handleSubmit = (e) => {
        (e).preventDefault()

        fetch(`https://morning-spire-84095.herokuapp.com/api/products/${productId}/update`, {

            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            },

            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(response => {

            console.log(response)
            setName(response.name)
			setDescription(response.description)
			setPrice(response.price)

            if(response){

                alert('Product successfully updated!')

                navigate('/products')
            }

        })
    }





    return(

        <div className='updateProduct'>
        <Container className="updateProductContainer m-5">
         <h1 className="text-center">Update Product</h1>
        <Form onSubmit={ (e) => handleSubmit(e) }>
            <Row>
                <Col>
                    <Form.Group className="mb-3">
                        <Form.Control
                            placeholder={name}
                            type="text" 
                            value={name}
                            onChange={ (e) => {setName(e.target.value);
                            setIsDisabled(false); }}
                            
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Control
                            placeholder="Product Description"
                            type="text"
                            as="textarea"
							rows={3}  
                            value={description}
                            onChange={ (e) => {setDescription(e.target.value); setIsDisabled(false); }}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Control
                            placeholder="Product Price"
                            type="number" 
                            value={price}
                            onChange={ (e) => {setPrice(e.target.value); setIsDisabled(false); }}
                            
                        />
                    </Form.Group>
                    
                    <div className='createProductButton'>
                        <Button type="submit" className="btn btn-info" disabled={isDisabled}>Update Product</Button>
                        <Link className="btn btn-info" to={'../products'}>Back</Link>
                    </div>

            </Col>
            </Row>
        </Form>
    </Container>
    </div>
    )
}