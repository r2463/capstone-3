import { Fragment, useEffect, useContext, useState } from 'react'
import { CardGroup, Row } from 'react-bootstrap'
import ProductCard from '../components/ProductCard'
import UserContext from '../UserContext'
import AdminView from './AdminView'


const token = localStorage.getItem('token')
const admin = localStorage.getItem('admin')

export default function Products (productProp) {


    const {state, dispatch} = useContext(UserContext)
    // console.log(state)

    const [products, setProducts] = useState([])

    useEffect(() => {


        
        fetch('https://morning-spire-84095.herokuapp.com/api/products/isActive', {
        
            method: "GET",


        })
        .then(response => response.json())
        .then(response => {

            if(token !== null){

                dispatch({type: "USER", payload: true})
            }

            setProducts(

                response.map(product => {
                    
                    return (

                    <ProductCard key={product._id} productProp={product}/>

                    )

                })
            )
        }) 
      
    }, [])

    
    return(

        <Fragment>
      
            {
            admin === "true" ?
                <Fragment>
                    <AdminView />
                </Fragment>
            :
                <CardGroup className='justify-content-center m-3'>
                    <Row className='text-center m-2' lg={3} md={2} xs={12}>
                    {products}
                    </Row>
                </CardGroup>
			}

		</Fragment>
        
    )

    }