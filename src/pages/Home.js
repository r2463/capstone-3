import {Fragment, useContext, useEffect } from 'react'
import location from '../images/location.jpg'
import Banner from '../components/Banner'
import UserContext from '../UserContext'
const token = localStorage.getItem('token')



export default function Home(){


    const { state, dispatch} = useContext(UserContext)

    
    useEffect( () => {
		if(token){
			dispatch({type: "USER", payload: true})
		} else {
			dispatch({type: "USER", payload: null})
		}
	}, [])


    const data = {

        title: "Ghost Kitchen",
        description: "Trayed meals for your celebrations and cravings",
        destination: "/products",
        buttonDesc: "Order Now"
    }

    return(

   <Fragment>
       <div>
            <Banner bannerProp={data}/>
       </div>
       <div className='home-2'>
           <h1>About Us</h1>
           <p>We offer reasonably-priced trayed meals perfect for your Celebrations, Gatherings, and Food Cravings. Our philosophy is simple: to provide great food, exceptional value, and friendly service to each and every customer we serve. </p>
       </div>
       <div className='home-3'>
            <div className='home-3Img'>
                <img src={location} alt='location.jpg'/>
            </div>
            <div className='home-3Text'>
                <p>Order for Pick Up or Book a Delivery Rider</p>
            </div>

       </div>
       
   </Fragment>


        
    )

    }