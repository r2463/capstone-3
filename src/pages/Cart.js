import { Fragment, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Button, Text } from 'react-bootstrap'
import { Add, Remove, DeleteOutline } from '@mui/icons-material'
import pasta from '../images/pasta.png'
import { Link } from 'react-router-dom'
import { removeFromCart, decreaseCart, increaseCart, clearCart } from '../redux/cartRedux'


export default function Cart() {

    const cart = useSelector(state => state.cart)
    const cartQuantity = useSelector(state => state.cart.cartQuantity)
    
    const dispatchCart = useDispatch()


    // REMOVE FROM CART BUTTON
    const handleRemoveFromCart = (product) => {
        dispatchCart(removeFromCart(product));
    }
   
    
     // QUANTITY BUTTON    
    const handleDecreaseCart = (product) =>{
        dispatchCart(decreaseCart(product));
    }

    const handleIncreaseCart = (product) =>{
        dispatchCart(increaseCart(product));
    }


    const handleClearCart = () =>{
        dispatchCart(clearCart());
    }
    





    return (

        <div className='Cart'>
            <div className="cartContainer">
                <h2 className="cartTitle">YOUR CART</h2>
                <div className='cartButtons'>

                    <Link to={'/products'}>
                        <Button className='cartButton1'>Continue Shopping</Button>
                    </Link>

                    <div>Shopping Cart({cartQuantity})</div>

                    <Link to={'/products'}>
                        <Button className='cartButton2 btn'>Checkout Now</Button>
                    </Link>


                </div>
                <Fragment>
                    {
                        cartQuantity > 0 ?

                            <Fragment>
                                <div className='cartBottom'>
                                    <div className='cartInfo'>
                                        {cart.products.map(product => (<div className='cartProduct' key={product._id}>
                                            <div className='cartProductDetail'>

                                                <img src={pasta} className='cartImage' alt='product' />

                                                <div className='cartDetail'>
                                                    <span><b>Product:</b> {product.name}</span>
                                                    <DeleteOutline onClick={() => handleRemoveFromCart(product)} />
                                                </div>
                                            </div>

                                            <div className='cartPrice'>
                                                <div className='cartQuantity'>
                                                    <Remove fontSize='small' onClick={() => handleDecreaseCart(product)} />
                                                    <div className='mx-2'>{product.quantity}</div>
                                                    <Add fontSize='small' onClick={() => handleIncreaseCart(product)} />
                                                </div>
                                                <div className='cartPriceDetail'>{product.price}</div>
                                            </div>
                            
                                        </div>))}
                                        <hr />

                                        <Button className='cartButton1' onClick={() => handleClearCart()}>Remove All</Button>

                                    </div>

                                    <div className='cartSummary'>
                                        <h1 className='summaryTitle'>ORDER SUMMARY</h1>
                                        <div className='summaryPrice'>
                                            <span>Subtotal</span>
                                            <span>{cart.totalPrice}</span>
                                        </div>
                                        <div className='summaryPrice font-weight-bold'>
                                            <span>Total</span>
                                            <span>{cart.totalPrice}</span>
                                        </div>
                                        <div>
                                            <Button className='summaryButton'>CHECKOUT NOW</Button>
                                        </div>
                                    </div>
                                </div>
                            </Fragment>

                            :

                            <Fragment>
                                <p className='cartIsEmpty'>Cart is Empty</p>

                            </Fragment>

                    }
                </Fragment>

            </div>
        </div>


    )
}