import { useState, useEffect } from 'react'
import { Form, Button, FloatingLabel } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom'


export default function Home () {


    const [fn, setFn] = useState("")
    const [ln, setLn] = useState("")
    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [vpw, setVpw] = useState("")
    const[isDisabled, setIsDisabled] = useState(true)

    const navigate = useNavigate()


     // useEffect(function, options)
    useEffect (() => {
        console.log('render')

        if(fn !== "" && ln !== "" && email !== "" && pw !== "" && vpw !== ""){

            setIsDisabled(false)
        }
        else{
            setIsDisabled(true)
        }

    }, [fn, ln, email, pw, vpw])

    
    const registerUser = (e) => {
        e.preventDefault()

        
        fetch('https://morning-spire-84095.herokuapp.com/api/users/email-exists', {

            method: "POST",
            headers: {
                "Content-Type": "application/json"

            },
            body: JSON.stringify({

                email: email

            })
        })
        .then(response => response.json())
        .then(response => {

            console.log(response)
            if(!response){

                fetch('https://morning-spire-84095.herokuapp.com/api/users/register', {

                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"

                    },
                    body: JSON.stringify({

                        firstName: fn,
                        lastName: ln,
                        email: email,
                        password: pw

                    })
                })
                .then(response => response.json())
                .then(response => {

                    console.log(response)
                    if(response){
                        alert(`Registeration Successfull!`)
            
                        navigate('../login')
            
                    }
                    else{
                        alert(`Something went wrong. Please try again`)
            
                    }
                })
            }



        })
        
    }







    
    return(

        <div className='Register'>
            <div className= "registerContainer">
            <h2 className='text-center'>Register</h2>
            <div className="row justify-content-center  col={12}">
                <div className="col">
                    <Form className="registerForm mb-3" onSubmit={(e) => registerUser(e)}>
                        <Form.Group className="mb-2">
                            <Form.Label>Firstname</Form.Label>
                            <Form.Control className='form-control form-control-sm' type="text" value ={fn} onChange = {(e) => setFn(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-2">
                            <Form.Label>Lastname</Form.Label>
                            <Form.Control className='form-control form-control-sm' type="text" value={ln} onChange={(e) => setLn(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-2">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control className='form-control form-control-sm' type="email" placeholder="example@mail.com" value={email} onChange = {(e) => setEmail(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-2">
                            <Form.Label>Password</Form.Label>
                            <Form.Control className='form-control form-control-sm' type="password" value={pw} onChange = {(e) => setPw(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Verify Password</Form.Label>
                            <Form.Control className='form-control form-control-sm' type="password" value={vpw} onChange = {(e) => setVpw(e.target.value)}/>
                        </Form.Group>

                        <Button className='btn-sm btn-block' variant="primary" type="submit"disabled={isDisabled}>REGISTER</Button>
                    </Form>
                </div>
            </div>
            </div>
        </div>



    )

    }