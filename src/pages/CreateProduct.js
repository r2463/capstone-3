import { useState, useEffect, useContext } from 'react'
import { Container, Form, Row, Col, Button } from 'react-bootstrap'
import { useNavigate, Link } from 'react-router-dom'
import UserContext from '../UserContext'

const token = localStorage.getItem('token')



export default function CreateProduct() {

    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [price, setPrice] = useState(0)
	const [isDisabled, setIsDisabled] = useState(true)

    const navigate = useNavigate()
    const {state, dispatch} = useContext(UserContext)


	// USEEFFECT HOOK FOR UPDATING USER REDUCER
    useEffect(() => {
		if(token !== null){

			dispatch({type: "USER", payload: true})
		}
	}, [])



	//  FETCHING API
    const handleSubmit = (e) => {
        (e).preventDefault()

        fetch('https://morning-spire-84095.herokuapp.com/api/products/create', {

            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
        
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(response => {
            // console.log(localStorage.getItem('admin'))

            if(response){

                alert('Product successfully added!')

                navigate('/products')
            }
        })
    }

	// USEEFFECT HOOK FOR UPDATING UPDATE BUTTON STATE
	useEffect(() => {
		console.log(`render`) 
 
		 if(name !== "" && description !== "" && price !== ""){
 
			 setIsDisabled(false)
		 }
		 else {
			 setIsDisabled(true)
		 }
	 }, [name, description, price])

    return(

        <div className='addProduct'>
			<Container className="addProductContainer m-5">
		 	<h1 className="text-center">Add Product</h1>
			<Form onSubmit={ (e) => handleSubmit(e) }>
				<Row>
					<Col>
						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Product Name"
					    		type="text" 
					    		value={name}
					    		onChange={ (e) => setName(e.target.value) }
					    		
					    	/>
						</Form.Group>

						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Product Description"
					    		type="text"
								as="textarea"
								rows={3} 
					    		value={description}
					    		onChange={ (e) => setDescription(e.target.value) }
					    	/>
						</Form.Group>

						<Form.Group className="mb-3">
					    	<Form.Control
					    		placeholder="Product Price"
					    		type="number" 
					    		value={price}
					    		onChange={ (e) => setPrice(e.target.value) }
					    		
					    	/>
						</Form.Group>
				
						<div className='createProductButton'>
						<Button type="submit" className="btn btn-info" disabled={isDisabled}>Add Product</Button>
						<Link className="btn btn-info" to={'../products'}>Back</Link>
						</div>

					</Col>
				</Row>
			</Form>
		</Container>
		</div>
    )
}