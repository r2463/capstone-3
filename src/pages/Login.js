import { useState, useEffect, useContext } from 'react'
import { useNavigate } from 'react-router-dom'
import {Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext'


export default function Login () {

    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    const {state, dispatch} = useContext(UserContext)
    const navigate = useNavigate()


    useEffect(() => {
       console.log(`render`) 

        if(email !== "" && pw !== ""){

            setIsDisabled(false)
        }
        else {
            setIsDisabled(true)
        }
    }, [email, pw])


    const loginUser = (e) => {
        e.preventDefault()

        fetch("https://morning-spire-84095.herokuapp.com/api/users/login", {

            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({

                email: email,
                password: pw
            })
        })
        .then(response => response.json())
        .then(response => {

            if(response){

                localStorage.setItem('token', response.token)
                const token = localStorage.getItem('token')

                fetch('https://morning-spire-84095.herokuapp.com/api/users/profile', {

                    method: "GET",
                    headers: {
                        "Authorization": `Bearer: ${token}`
                    }
                })
                .then(response => response.json())
                .then(response => {

                    localStorage.setItem('admin', response.isAdmin)
                    
                    dispatch({type: "USER", payload: true })
                })

                setEmail("")
                setPw("")

                navigate('../')
            }
            else{

                alert(`Your email and/or password is incorrect. Please try again`)
            }

        })

    }

    return(

        <div className='Login'>
            <div className= "LoginContainer m-5">
            <h2 className='text-center'>Login</h2>
            <div className="row justify-content-center">
                <div className="col-10">
                    <Form className='loginForm mb-3' onSubmit={(e) => loginUser(e)}>
                        <Form.Group className="mb-2">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control className='form-control form-control-sm' type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-4">
                            <Form.Label>Password</Form.Label>
                            <Form.Control className='form-control form-control-sm' type="password" value={pw} onChange={(e) => setPw(e.target.value)} />
                        </Form.Group>

                        
                        <Button className='btn-sm btn-block btn-info' variant="secondary" type="submit" disabled={isDisabled}>LOGIN</Button>

                    </Form>
                </div>
            </div>
            </div>
        </div>
    )

}