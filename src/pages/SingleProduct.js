import { useContext, useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { Card, Button } from 'react-bootstrap'
import { Remove, Add } from '@mui/icons-material'
import UserContext from "../UserContext";
import pasta from '../images/pasta.png'
import { addProduct } from '../redux/cartRedux'


const token = localStorage.getItem('token')



export default function SingleProduct(){


    const {state, dispatch} = useContext(UserContext)


    const [product, setProduct] = useState({})
    const [quantity, setQuantity] = useState(1)

    const {productId} = useParams()
    const navigate = useNavigate()
    const dispatchCart = useDispatch()

    


    const fetchProduct = () => {

        fetch(`https://morning-spire-84095.herokuapp.com/api/products/${productId}`, {

            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(response => {

            setProduct(response)
            // console.log(product)

        })
    }

    // USE EFFECT HOOK
    useEffect(() => {

        if(token !== null){

            dispatch({type: "USER", payload: true})

        }
        fetchProduct()
    }, [])


    // QUANTITY BUTTON
    const handleQuantity = (type) => {

        if(type === 'dec'){

            quantity >1 && setQuantity(quantity -1)
        }
        else {
            setQuantity(quantity +1)

        }
    }


    // ADD TO CART BUTTON
    const handleAddToCart = () => {
        dispatchCart(addProduct({ ...product, quantity }));

    }


    return(

        <div className='d-flex my-5 px-5'>
            <div>
                <img src={pasta} className='img-fluid' alt='product'/>

            </div>
            <div className="container">
                <Card className="m-5">
                <Card.Body>
                    <Card.Title>{product.name}</Card.Title>
                    <Card.Text>
                    {product.description}
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>
                        {product.price}
                    </Card.Text>
                    <div className='quantityContainer d-flex'>
                        <Remove onClick={ () =>handleQuantity('dec') }/>
                        <span>{quantity}</span>
                        <Add onClick={ () =>handleQuantity('inc') }/>
                    </div>

                    <Button 
                        className="productButton btn " 
                        onClick={ () => handleAddToCart() }
                    >Add to Cart</Button>
                </Card.Body>
                </Card>
            </div>
        </div>

    )
}